package pl.bar;

import java.util.List;

public interface Bar {
    public abstract void create(Meal meal);

    public abstract Meal findMeal(int id);

    public abstract List<Meal> getMeals();

    public abstract void update(Meal meal);

    public abstract void deleteMeal(int id);


    public abstract void create(Customer customer);

    public abstract Customer findCustomer(int id);

    public abstract List<Customer> getCustomers();

    public abstract void update(Customer customer);

    public abstract void deleteCustomer(int id);


    public abstract void create(Ingredient ingredient);

    public abstract Ingredient findIngredient(int id);

    public abstract List<Ingredient> getIngredients();

    public abstract void update(Ingredient ingredient);

    public abstract void deleteIngredient(int id);


    public abstract void create(MealOrder mealOrder);

    public abstract MealOrder findMealOrder(int id);

    public abstract List<MealOrder> getMealOrders();

    public abstract void update(MealOrder order);

    public abstract void deleteMealOrder(int id);
}
