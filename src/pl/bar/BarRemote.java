package pl.bar;

import javax.xml.bind.JAXB;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

public class BarRemote implements Bar {
    String url = "http://localhost:8080/take/bar";

    @Override
    public void create(Meal meal) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(meal, stringWriter);
        HttpHelper.doPost(url+"/createMeal", stringWriter.toString(), "application/xml");
    }

    @Override
    public void create(Customer customer) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(customer, stringWriter);
        HttpHelper.doPost(url+"/createCustomer", stringWriter.toString(), "application/xml");
    }

    @Override
    public void create(Ingredient ingredient) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(ingredient, stringWriter);
        HttpHelper.doPost(url+"/createIngredient", stringWriter.toString(), "application/xml");
    }

    @Override
    public void create(MealOrder mealOrder) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(mealOrder, stringWriter);
        HttpHelper.doPost(url+"/createMealOrder", stringWriter.toString(), "application/xml");
    }

    @Override
    public void deleteMeal(int id) {
        HttpHelper.doGet(url+"/deleteMeal/"+id);
    }

    @Override
    public void deleteCustomer(int id) {
        HttpHelper.doGet(url+"/deleteCustomer/"+id);
    }

    @Override
    public void deleteIngredient(int id) {
        HttpHelper.doGet(url+"/deleteIngredient/"+id);
    }

    @Override
    public void deleteMealOrder(int id) {
        HttpHelper.doGet(url+"/deleteMealOrder/"+id);
    }

    @Override
    public Meal findMeal(int id) {
        String txt = HttpHelper.doGet(url+"/findMeal/"+id);
        if (txt.length() != 0) {
            Meal meal =  JAXB.unmarshal(new StringReader(txt), Meal.class);
            return meal;
        }

        return null;
    }

    @Override
    public Customer findCustomer(int id) {
        String txt = HttpHelper.doGet(url+"/findCustomer/"+id);
        if (txt.length() != 0) {
            Customer customer =  JAXB.unmarshal(new StringReader(txt), Customer.class);
            return customer;
        }

        return null;
    }

    @Override
    public Ingredient findIngredient(int id) {
        String txt = HttpHelper.doGet(url+"/findIngredient/"+id);
        if (txt.length() != 0) {
            Ingredient ingredient =  JAXB.unmarshal(new StringReader(txt), Ingredient.class);

            return ingredient;
        }

        return null;
    }

    @Override
    public MealOrder findMealOrder(int id) {
        String txt = HttpHelper.doGet(url+"/findMealOrder/"+id);
        if (txt.length() != 0) {
            MealOrder mealOrder =  JAXB.unmarshal(new StringReader(txt), MealOrder.class);

            return mealOrder;
        }

        return null;
    }

    @Override
    public List<Meal> getMeals() {
        String txt = HttpHelper.doGet(url+"/getMeals");
        Meals meals = JAXB.unmarshal(new StringReader(txt), Meals.class);

        return meals.getMeal();
    }

    @Override
    public List<Customer> getCustomers(){
        String txt = HttpHelper.doGet(url+"/getCustomers");
        Customers customers = JAXB.unmarshal(new StringReader(txt), Customers.class);

        return customers.getCustomers();
    }

    @Override
    public List<Ingredient> getIngredients(){
        String txt = HttpHelper.doGet(url+"/getIngredients");
        Ingredients ingredients = JAXB.unmarshal(new StringReader(txt), Ingredients.class);

        return ingredients.getIngredient();
    }

    @Override
    public List<MealOrder> getMealOrders(){
        String txt = HttpHelper.doGet(url+"/getMealOrders");
        MealOrders mealOrders = JAXB.unmarshal(new StringReader(txt), MealOrders.class);

        return mealOrders.getMealOrders();
    }

    @Override
    public void update(Meal meal) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(meal, stringWriter);
        HttpHelper.doPost(url+"/updateMeal", stringWriter.toString(), "application/xml");
    }

    @Override
    public void update(Customer customer) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(customer, stringWriter);
        HttpHelper.doPost(url+"/updateCustomer", stringWriter.toString(), "application/xml");
    }

    @Override
    public void update(Ingredient ingredient) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(ingredient, stringWriter);
        HttpHelper.doPost(url+"/updateIngredient", stringWriter.toString(), "application/xml");
    }

    @Override
    public void update(MealOrder mealOrder) {
        StringWriter stringWriter = new StringWriter();
        JAXB.marshal(mealOrder, stringWriter);
        HttpHelper.doPost(url+"/updateMealOrder", stringWriter.toString(), "application/xml");
    }

}
