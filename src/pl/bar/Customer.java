package pl.bar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@XmlRootElement(name = "customer")
public class Customer implements Serializable {
    int id;
    int tableId;
    String firstName;
    String lastName;
    Set<MealOrder> mealOrders = new HashSet<MealOrder>();;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTableId() {
        return tableId;
    }

    public void setTableId(int tableId) {
        this.tableId = tableId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<MealOrder> getMealOrders() {
        return mealOrders;
    }

    public void setMealOrders(Set<MealOrder> mealOrders) {
        this.mealOrders = mealOrders;
    }

    @Override
    public String toString() {
        return this.firstName + " | " + this.lastName + " | " + this.tableId;
    }
}
