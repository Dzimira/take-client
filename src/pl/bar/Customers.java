package pl.bar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "collection")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Customers implements Serializable {
    /**
     * List of customers
     */
    List<Customer> customers = new ArrayList<Customer>();

    /**
     * Constructor with one parameter
     *
     * @param customers is list of customers
     */
    public Customers(List<Customer> customers) {
        super();
        this.customers = customers;
    }

    /**
     * Constructor withput parameters
     */
    public Customers() {}

    /**
     * Gets list of customers
     *
     * @return list of customers
     */
    @XmlElement(name = "customer")
    public List<Customer> getCustomers() {
        return customers;
    }

    /**
     * Sets list of customers
     *
     * @param customers list of customers
     */
    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }
}
