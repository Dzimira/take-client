package pl.bar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@XmlRootElement(name = "ingredient")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Ingredient implements Serializable {
    int idi;
    String name;
    Set<Meal> meals = new HashSet<Meal>();

    public int getIdi() {
        return idi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    public void setIdi(int idi) {
        this.idi = idi;
    }

    public void addMeal(Meal meal) {
        this.meals.add(meal);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
