package pl.bar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "collection")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Ingredients implements Serializable {

    List<Ingredient> ingredient = new ArrayList<Ingredient>();

    public Ingredients(List<Ingredient> ingredient) {
        super();
        this.ingredient = ingredient;
    }

    /**
     * Constructor withput parameters
     */
    public Ingredients() {}

    /**
     * Gets list of meals
     *
     * @return list of meals
     */
    public List<Ingredient> getIngredient() {
        return ingredient;
    }

    /**
     * Sets list of meals
     *
     * @param ingredients list of meals
     */
    public void setIngredient(List<Ingredient> ingredients) {
        this.ingredient = ingredients;
    }
}
