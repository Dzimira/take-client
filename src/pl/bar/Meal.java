package pl.bar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@XmlRootElement(name = "meal")
@XmlAccessorType(XmlAccessType.FIELD)
public class Meal implements Serializable {

    private static final long serialVersionUID = 1L;

    int id;
    String name;
    String description;
    Double price;
    String mealType;
    Set<Ingredient> ingredients = new HashSet<Ingredient>();
    Set<MealOrder> mealOrders = new HashSet<MealOrder>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMealType() {
        return mealType;
    }

    public void setMealType(String mealType) {
        this.mealType = mealType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @XmlTransient
    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public void addIngredient(Ingredient ingredient) {
        this.ingredients.add(ingredient);
    }

    @XmlTransient
    public Set<MealOrder> getMealOrders() {
        return mealOrders;
    }

    public void setMealOrders(Set<MealOrder> mealOrders) {
        this.mealOrders = mealOrders;
    }

    public void addMealOrders(MealOrder mealOrder) {
        this.mealOrders.add(mealOrder);
    }

    @Override
    public String toString() {
        return this.name + " | " + this.description + " | " + this.price + "zł | " + this.mealType;
    }
}
