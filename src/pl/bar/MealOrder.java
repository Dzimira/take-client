package pl.bar;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@XmlRootElement(name = "mealOrder")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MealOrder implements Serializable {
    int ido;
    Double price;
    Customer customer;
    Set<Meal> meals = new HashSet<Meal>();

    public int getIdo() {
        return ido;
    }

    public void setIdo(int ido) {
        this.ido = ido;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

   @XmlTransient
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @XmlTransient
    public Set<Meal> getMeals() {
        return meals;
    }

    public void setMeals(Set<Meal> meals) {
        this.meals = meals;
    }

    public void addMeal(Meal meal) {
        this.meals.add(meal);
    }

    @Override
    public String toString() {
        return "Kwota zamówienia: " + this.price + "zł";
    }
}
