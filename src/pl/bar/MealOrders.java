package pl.bar;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "collection")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MealOrders implements Serializable {
    List<MealOrder> mealOrders = new ArrayList<MealOrder>();

    public MealOrders() {}

    public MealOrders(List<MealOrder> mealOrders) {
        super();
        this.mealOrders = mealOrders;
    }

    @XmlElement(name = "mealOrder")
    public List<MealOrder> getMealOrders() {
        return mealOrders;
    }

    public void setMealOrders(List<MealOrder> mealOrders) {
        this.mealOrders = mealOrders;
    }
}
