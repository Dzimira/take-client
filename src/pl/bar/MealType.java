package pl.bar;

import java.io.Serializable;

public enum MealType implements Serializable {
    /**
     * The meal is a meal breakfast
     */
    BREAKFAST,

    /**
     * The meal is a meal dinner
     */
    DINNER,

    /**
     * The meal is a evening meal
     */
    EVENING_MEAL;

    /**
     * Overidden method which returns a full name of the meal type
     *
     * @return a string of the full status name
     */
    @Override
    public String toString() {
        switch (this) {
            case BREAKFAST:
                return "breakfast";
            case DINNER:
                return "dinner";
            case EVENING_MEAL:
                return "evening_meal";
            default:
                return "Unknown string: " + this;
        }
    }
}
