package pl.bar;

import com.oracle.webservices.internal.api.message.PropertySet;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "collection")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Meals implements Serializable {
    /**
     * List of meals
     */
    private List<Meal> meal = new ArrayList<Meal>();

    public Meals(List<Meal> meals) {
        super();
        this.meal = meal;
    }

    public Meals() {
    }

    /**
     * Gets list of meals
     *
     * @return list of meals
     */
    @XmlElement(name = "meal")
    public List<Meal> getMeal() {
        return meal;
    }

    public void setMeal(List<Meal> meal) {
        this.meal = meal;
    }
}
