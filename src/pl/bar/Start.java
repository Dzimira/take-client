package pl.bar;

import pl.bar.View.BarWindow;

import java.awt.*;

public class Start {
    public static void main(String[] args) {
        BarWindow window = new BarWindow();
        window.init();
    }
}
