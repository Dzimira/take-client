package pl.bar.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BarWindow extends JFrame {
    private static final long serialVersionUID = 1L;

    Dimension windowSize;
    GridBagConstraints gridBagConstraints;

    public void init() {
        this.windowSize = new Dimension(400, 300);
        this.gridBagConstraints = new GridBagConstraints();

        JPanel buttonPanel = new JPanel(new GridBagLayout());
        buttonPanel.setPreferredSize(windowSize);

        this.setSize(this.windowSize);
        this.centeringWindow();
        this.setTitle("Bar");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JButton meal = new JButton("Meal CRUD");
        JButton customer = new JButton("Customer CRUD");
        JButton mealOrder = new JButton("Order CRUD");
        JButton ingredient = new JButton("Ingredient CRUD");

        meal.setPreferredSize(new Dimension(300,30));
        customer.setPreferredSize(new Dimension(300,30));
        mealOrder.setPreferredSize(new Dimension(300,30));
        ingredient.setPreferredSize(new Dimension(300,30));

        meal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MealCRUD mealCRUD = new MealCRUD();
            }
        });

        ingredient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                IngredientCRUD ingredientCRUD = new IngredientCRUD();
            }
        });

        customer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CustomerCRUD customerCRUD = new CustomerCRUD();
            }
        });

        mealOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MealOrderCRUD mealOrderCRUD = new MealOrderCRUD();
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);
        gridBagConstraints.gridy = 0;
        buttonPanel.add(meal, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        buttonPanel.add(customer, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        buttonPanel.add(mealOrder, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        buttonPanel.add(ingredient, gridBagConstraints);

        getContentPane().add(buttonPanel, BorderLayout.CENTER);
        this.setVisible(true);
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
