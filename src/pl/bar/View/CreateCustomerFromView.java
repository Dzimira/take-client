package pl.bar.View;

import pl.bar.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class CreateCustomerFromView extends JFrame {
    Dimension windowSize;
    GridBagConstraints gridBagConstraints;
    Bar barDao;

    public CreateCustomerFromView() {
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.setVisible(true);
        this.setLayout(new GridBagLayout());
        this.setTitle("Add new customer");
        this.gridBagConstraints = new GridBagConstraints();
        this.barDao = new BarRemote();
        this.centeringWindow();

        prepareForm();
    }

    protected void prepareForm() {
        JLabel firstNameLabel = new JLabel("First name:");
        JLabel lastNameLabel = new JLabel("Last name:");
        JLabel tableLabel = new JLabel("TableId:");
        JLabel orderLabel = new JLabel("Order:");
        JTextField priceField = new JTextField();
        JTextField tableField = new JTextField();
        JTextField firstNameField = new JTextField();
        JTextField lastNameField = new JTextField();
        ArrayList<MealOrder> mealOrders = (ArrayList<MealOrder>) barDao.getMealOrders();
        MealOrder[] mealOrder = mealOrders.toArray(new MealOrder[mealOrders.size()]);
        JComboBox<MealOrder> mealOrderJComboBox = new JComboBox<MealOrder>(mealOrder);
        mealOrderJComboBox.setRenderer(new ListCellRednderer());
        JButton confirm = new JButton("Confirm");

        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int table = Integer.parseInt(tableField.getText());
                String firstName = firstNameField.getText();
                String lastName = lastNameField.getText();

                Customer customer = new Customer();
                customer.setTableId(table);
                customer.setFirstName(firstName);
                customer.setLastName(lastName);
                barDao.create(customer);
                dispose();
                JOptionPane.showMessageDialog(null, "Customer has been successfully added!");
            }
        });

        Dimension fieldSize = new Dimension(350, 30);
        tableField.setPreferredSize(fieldSize);
        priceField.setPreferredSize(fieldSize);

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);

        gridBagConstraints.gridy = 0;
        this.add(firstNameLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(firstNameField, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(lastNameLabel, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        this.add(lastNameField, gridBagConstraints);
        gridBagConstraints.gridy = 4;
        this.add(tableLabel, gridBagConstraints);
        gridBagConstraints.gridy = 5;
        this.add(tableField, gridBagConstraints);
        gridBagConstraints.gridy = 6;
        this.add(orderLabel, gridBagConstraints);
        gridBagConstraints.gridy = 7;
        this.add(mealOrderJComboBox, gridBagConstraints);
        gridBagConstraints.gridy = 8;
        this.add(confirm, gridBagConstraints);
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
