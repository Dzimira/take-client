package pl.bar.View;

import pl.bar.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;
import java.util.List;

public class CreateIngredientFromView extends JFrame {

    Dimension windowSize;
    GridBagConstraints gridBagConstraints;
    Bar barDao;
    JList<Meal> mealJList;
    DefaultListModel defaultListModel;
    JScrollPane pane;

    public CreateIngredientFromView() {
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.setVisible(true);
        this.setLayout(new GridBagLayout());
        this.setTitle("Add new ingredient");
        this.gridBagConstraints = new GridBagConstraints();
        this.barDao = new BarRemote();
        this.centeringWindow();
        this.pane = new JScrollPane();

        prepareForm();
    }

    protected void prepareForm() {
        this.defaultListModel = new DefaultListModel();
        prepareList();
        this.mealJList = new JList<>(defaultListModel);
        this.mealJList.setFixedCellHeight(30);
        this.mealJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        this.mealJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(mealJList);

        JLabel nameLabel = new JLabel("Name:");
        JLabel mealIngredientLabel = new JLabel("Meal:");
        JTextField nameField = new JTextField();
        JButton confirm = new JButton("Confirm");

        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameField.getText();
                List<Meal> meals = mealJList.getSelectedValuesList();
                Ingredient ingredient = new Ingredient();
                ingredient.setName(name);

                for (Meal meal : meals) {
                    meal.addIngredient(ingredient);
                    ingredient.addMeal(meal);
                }

                barDao.create(ingredient);
                dispose();
                JOptionPane.showMessageDialog(null, "Ingredient has been successfully added!");
            }
        });

        Dimension fieldSize = new Dimension(350, 30);
        nameField.setPreferredSize(fieldSize);
        pane.setPreferredSize(new Dimension(350, 100));

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);

        gridBagConstraints.gridy = 0;
        this.add(nameLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(nameField, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(mealIngredientLabel, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 4;
        this.add(confirm, gridBagConstraints);
    }

    private void prepareList() {
        defaultListModel.removeAllElements();

        for (Meal meal : barDao.getMeals()) {
            defaultListModel.addElement(meal);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}