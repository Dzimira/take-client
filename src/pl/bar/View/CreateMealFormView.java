package pl.bar.View;

import pl.bar.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class CreateMealFormView extends JFrame {
    Dimension windowSize;
    GridBagConstraints gridBagConstraints;
    Bar barDao;
    JList<Ingredient> ingredientList;
    DefaultListModel defaultListModel;
    JScrollPane pane;

    public CreateMealFormView() {
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.setVisible(true);
        this.setLayout(new GridBagLayout());
        this.setTitle("Add new meal");
        this.pane = new JScrollPane();
        this.gridBagConstraints = new GridBagConstraints();
        this.barDao = new BarRemote();
        this.centeringWindow();

        prepareForm();
    }

    protected void prepareForm() {
        this.defaultListModel = new DefaultListModel();
        prepareList();
        this.ingredientList = new JList<>(defaultListModel);
        this.ingredientList.setFixedCellHeight(30);
        this.ingredientList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        this.ingredientList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(ingredientList);

        String[] mealTypes = {
                MealType.BREAKFAST.toString(),
                MealType.DINNER.toString(),
                MealType.EVENING_MEAL.toString()
        };
        JComboBox mealTypeList = new JComboBox(mealTypes);
        mealTypeList.setSelectedIndex(0);
        JLabel nameLabel = new JLabel("Name:");
        JLabel descriptionLabel = new JLabel("Description:");
        JLabel priceLabel = new JLabel("Price:");
        JLabel ingredientLabel = new JLabel("Ingredients:");
        JTextField nameField = new JTextField();
        JTextField descriptionField = new JTextField();
        JTextField priceField = new JTextField();
        JButton confirm = new JButton("Confirm");

        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = nameField.getText();
                String description = descriptionField.getText();
                Double price = Double.parseDouble(priceField.getText());
                String mealType = (String) mealTypeList.getSelectedItem();
                List<Ingredient> ingredients = ingredientList.getSelectedValuesList();

                Meal meal = new Meal();
                meal.setName(name);
                meal.setDescription(description);
                meal.setPrice(price);
                meal.setMealType(mealType);

                for (Ingredient ingredient : ingredients) {
                    ingredient.addMeal(meal);
                    meal.addIngredient(ingredient);
                    ingredient.addMeal(meal);
                    barDao.update(ingredient);
                }

                barDao.create(meal);
                dispose();
                JOptionPane.showMessageDialog(null, "Meal has been successfully added!");
            }
        });

        Dimension fieldSize = new Dimension(350, 30);
        nameField.setPreferredSize(fieldSize);
        descriptionField.setPreferredSize(fieldSize);
        priceField.setPreferredSize(fieldSize);
        mealTypeList.setPreferredSize(fieldSize);
        pane.setPreferredSize(new Dimension(350, 100));

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);

        gridBagConstraints.gridy = 0;
        this.add(nameLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(nameField, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(descriptionLabel, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        this.add(descriptionField, gridBagConstraints);
        gridBagConstraints.gridy = 4;
        this.add(priceLabel, gridBagConstraints);
        gridBagConstraints.gridy = 5;
        this.add(priceField, gridBagConstraints);
        gridBagConstraints.gridy = 6;
        this.add(ingredientLabel, gridBagConstraints);
        gridBagConstraints.gridy = 7;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 8;
        this.add(mealTypeList, gridBagConstraints);
        gridBagConstraints.gridy = 9;
        this.add(confirm, gridBagConstraints);
    }

    private void prepareList() {
        defaultListModel.removeAllElements();
        for (Ingredient ingredient : barDao.getIngredients()) {
            defaultListModel.addElement(ingredient);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
