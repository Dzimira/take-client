package pl.bar.View;

import pl.bar.*;

import javax.swing.*;
import java.awt.*;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class CreateMealOrderFormView extends JFrame{
    Dimension windowSize;
    GridBagConstraints gridBagConstraints;
    Bar barDao;
    JList<Meal> mealJList;
    DefaultListModel defaultListModel;
    JScrollPane pane;

    public CreateMealOrderFormView() {
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.setVisible(true);
        this.setLayout(new GridBagLayout());
        this.setTitle("Add new order");
        this.gridBagConstraints = new GridBagConstraints();
        this.barDao = new BarRemote();
        this.centeringWindow();
        this.pane = new JScrollPane();

        prepareForm();
    }

    protected void prepareForm() {
        this.defaultListModel = new DefaultListModel();
        prepareList();
        this.mealJList = new JList<>(defaultListModel);
        this.mealJList.setFixedCellHeight(30);
        this.mealJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        this.mealJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(mealJList);

        String[] mealTypes = {
                MealType.BREAKFAST.toString(),
                MealType.DINNER.toString(),
                MealType.EVENING_MEAL.toString()
        };
        JComboBox mealTypeList = new JComboBox(mealTypes);
        JLabel mealsLabel = new JLabel("Select meals for order:");
        mealTypeList.setSelectedIndex(0);
        JLabel customerLabel = new JLabel("Customer:");
        ArrayList<Customer> customer = (ArrayList<Customer>) barDao.getCustomers();
        Customer[] customers = customer.toArray(new Customer[customer.size()]);
        JComboBox<Customer> customerJComboBox = new JComboBox<Customer>(customers);

        JButton confirm = new JButton("Confirm");

        confirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Double price = 0.0;
                MealOrder mealOrder = new MealOrder();
                Customer selectedCustomer = (Customer) customerJComboBox.getSelectedItem();
                java.util.List<Meal> meals = mealJList.getSelectedValuesList();

                for (Meal meal : meals) {
                    price += meal.getPrice();
                    mealOrder.addMeal(meal);
                }

                mealOrder.setCustomer(selectedCustomer);
                mealOrder.setPrice(price);

                for (Meal meal : meals) {
                    meal.addMealOrders(mealOrder);
                }

                barDao.create(mealOrder);
                dispose();
                JOptionPane.showMessageDialog(null, "Order has been successfully added!");
            }
        });

        Dimension fieldSize = new Dimension(350, 30);
        pane.setPreferredSize(new Dimension(350, 100));
        mealTypeList.setPreferredSize(fieldSize);

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 10, 0);

        gridBagConstraints.gridy = 0;
        this.add(mealsLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(customerLabel, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        this.add(customerJComboBox, gridBagConstraints);
        gridBagConstraints.gridy = 4;
        this.add(confirm, gridBagConstraints);
    }

    protected void prepareList() {
        this.defaultListModel.removeAllElements();
        for (Meal meal : barDao.getMeals()) {
            defaultListModel.addElement(meal);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
