package pl.bar.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CustomerCRUD extends JFrame {
    Dimension windowSize;
    GridBagConstraints gridBagConstraints;

    public CustomerCRUD() {
        this.windowSize = new Dimension(400, 300);
        this.setTitle("Customer CRUD");
        this.setSize(windowSize);
        this.gridBagConstraints = new GridBagConstraints();
        this.setLayout(new GridBagLayout());
        this.setVisible(true);
        this.centeringWindow();

        prepareGui();
    }

    private void prepareGui() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(350, 200));

        JButton addBtn = new JButton("Add customer");
        JButton getBtn = new JButton("Show customer list");
        JButton findBtn = new JButton("Find customer by id");
        JButton removeBtn = new JButton("Remove customer");

        addBtn.setPreferredSize(new Dimension(300,30));
        getBtn.setPreferredSize(new Dimension(300,30));
        findBtn.setPreferredSize(new Dimension(300,30));
        removeBtn.setPreferredSize(new Dimension(300,30));

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateCustomerFromView createCustomerFormView = new CreateCustomerFromView();
            }
        });

        getBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CustomerListView customerListView = new CustomerListView();
            }
        });

        findBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FindCustomerByIdView findCustomerByIdView = new FindCustomerByIdView();
            }
        });

        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RemoveCustomerView removeCustomerView = new RemoveCustomerView();
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        buttonPanel.add(addBtn, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        buttonPanel.add(getBtn, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        buttonPanel.add(findBtn, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        buttonPanel.add(removeBtn, gridBagConstraints);

        gridBagConstraints.gridy = 0;
        this.add(buttonPanel, gridBagConstraints);
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
