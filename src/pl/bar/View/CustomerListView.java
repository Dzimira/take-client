package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.ListCellRednderer;
import pl.bar.Customer;

import javax.swing.*;
import java.awt.*;

public class CustomerListView extends JFrame {
    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<Customer> customerJList;
    private DefaultListModel customerListModel;
    private Bar barDao;
    JScrollPane pane;

    public CustomerListView() {
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        prepareGui();
    }

    private void prepareGui() {
        this.customerListModel = new DefaultListModel();
        prepareList();
        this.customerJList = new JList<>(customerListModel);
        this.customerJList.setFixedCellHeight(50);
        customerJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        customerJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(customerJList);
        JLabel headerLabel = new JLabel("List of customers (Price | TableId):");

        this.add(headerLabel, BorderLayout.NORTH);
        this.add(pane, BorderLayout.CENTER);
    }

    private void prepareList() {
        customerListModel.removeAllElements();
        for (Customer customer : barDao.getCustomers()) {
            customerListModel.addElement(customer);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
