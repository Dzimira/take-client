package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.Customer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FindCustomerByIdView extends JFrame{
    private Dimension windowSize;
    private Bar barDao;
    private JTextField customerIdField;
    private JButton findCustomerBtn;

    public FindCustomerByIdView() {
        this.barDao = new BarRemote();
        this.findCustomerBtn = new JButton("Find customer");
        this.customerIdField = new JTextField();
        this.setVisible(true);
        this.windowSize = new Dimension(300, 100);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.setTitle("Find customer by id");
        prepareGui();
    }

    private void prepareGui() {

        JLabel customerIdLabel = new JLabel("Enter customer id:");

        findCustomerBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int customerId = Integer.parseInt(customerIdField.getText());
                Customer customer = barDao.findCustomer(customerId);

                if (customer != null) {
                    JOptionPane.showMessageDialog(null, getCustomerData(customer));
                } else {
                    JOptionPane.showMessageDialog(null, "Customer was not found!");
                }

            }
        });

        this.add(customerIdLabel, BorderLayout.NORTH);
        this.add(customerIdField, BorderLayout.CENTER);
        this.add(findCustomerBtn, BorderLayout.SOUTH);
    }

    private String getCustomerData(Customer customer) {
        String customerData =
                "First name: " + customer.getFirstName() + "\n" +
                        "Last name: " + customer.getLastName() + "\n" +
                        "Table id: " + customer.getTableId();

        return customerData;
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
