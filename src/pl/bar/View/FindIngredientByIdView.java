package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.Ingredient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Mary on 18.06.2017.
 */
public class FindIngredientByIdView extends JFrame{
    private Dimension windowSize;
    private Bar barDao;
    private JTextField ingredientIdField;
    private JButton findIngredientBtn;

    public FindIngredientByIdView() {
        this.barDao = new BarRemote();
        this.findIngredientBtn = new JButton("Find ingredient");
        this.ingredientIdField = new JTextField();
        this.setVisible(true);
        this.windowSize = new Dimension(300, 100);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.setTitle("Find ingredient by id");
        prepareGui();
    }

    private void prepareGui() {

        JLabel ingredientIdLabel = new JLabel("Enter ingredient id:");

        findIngredientBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ingredientId = Integer.parseInt(ingredientIdField.getText());
                Ingredient ingredient = barDao.findIngredient(ingredientId);

                if (ingredient != null) {
                    JOptionPane.showMessageDialog(null, getIngredientData(ingredient));
                } else {
                    JOptionPane.showMessageDialog(null, "Ingredient was not found!");
                }

            }
        });

        this.add(ingredientIdLabel, BorderLayout.NORTH);
        this.add(ingredientIdField, BorderLayout.CENTER);
        this.add(findIngredientBtn, BorderLayout.SOUTH);
    }

    private String getIngredientData(Ingredient ingredient) {
        String ingredientData = "Name: " + ingredient.getName();

        return ingredientData;
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
