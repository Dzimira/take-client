package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.Meal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FindMealByIdView extends JFrame {
    private Dimension windowSize;
    private Bar barDao;
    private JTextField mealIdField;
    private JButton findMealBtn;

    public FindMealByIdView() {
        this.barDao = new BarRemote();
        this.findMealBtn = new JButton("Find meal");
        this.mealIdField = new JTextField();
        this.setVisible(true);
        this.windowSize = new Dimension(300, 100);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.setTitle("Find meal by id");
        prepareGui();
    }

    private void prepareGui() {

        JLabel mealIdLabel = new JLabel("Enter meal id:");

        findMealBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int mealId = Integer.parseInt(mealIdField.getText());
                Meal meal = barDao.findMeal(mealId);

                if (meal != null) {
                    JOptionPane.showMessageDialog(null, getMealData(meal));
                } else {
                    JOptionPane.showMessageDialog(null, "Meal was not found!");
                }

            }
        });

        this.add(mealIdLabel, BorderLayout.NORTH);
        this.add(mealIdField, BorderLayout.CENTER);
        this.add(findMealBtn, BorderLayout.SOUTH);
    }

    private String getMealData(Meal meal) {
        String mealData =
                "Name: " + meal.getName() + "\n" +
                "Description: " + meal.getDescription() + "\n" +
                "Price: " + meal.getPrice() + "zł" + "\n" +
                "Meal type: " + meal.getMealType();

        return mealData;
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
