package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.MealOrder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FindMealOrderByIdView extends JFrame{
    private Dimension windowSize;
    private Bar barDao;
    private JTextField mealOrderIdField;
    private JButton findMealOrderBtn;

    public FindMealOrderByIdView() {
        this.barDao = new BarRemote();
        this.findMealOrderBtn = new JButton("Find order");
        this.mealOrderIdField = new JTextField();
        this.setVisible(true);
        this.windowSize = new Dimension(300, 100);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.setTitle("Find order by id");
        prepareGui();
    }

    private void prepareGui() {

        JLabel mealOrderIdLabel = new JLabel("Enter order id:");

        findMealOrderBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int mealOrderId = Integer.parseInt(mealOrderIdField.getText());
                MealOrder mealOrder = barDao.findMealOrder(mealOrderId);

                if (mealOrder != null) {
                    JOptionPane.showMessageDialog(null, getMealOrderData(mealOrder));
                } else {
                    JOptionPane.showMessageDialog(null, "Order was not found!");
                }
            }
        });

        this.add(mealOrderIdLabel, BorderLayout.NORTH);
        this.add(mealOrderIdField, BorderLayout.CENTER);
        this.add(findMealOrderBtn, BorderLayout.SOUTH);
    }

    private String getMealOrderData(MealOrder mealOrder) {
        String mealOrderData =
                "Price: " + mealOrder.getPrice();

        return mealOrderData;
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
