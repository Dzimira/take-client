package pl.bar.View;

import pl.bar.*;

import javax.swing.*;
import java.awt.*;

public class IngredientListView extends JFrame{
    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<Ingredient> ingredientJList;
    private DefaultListModel ingredientListModel;
    private Bar barDao;
    JScrollPane pane;

    public IngredientListView() {
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        prepareGui();
    }

    private void prepareGui() {
        this.ingredientListModel = new DefaultListModel();
        prepareList();
        this.ingredientJList = new JList<>(ingredientListModel);
        this.ingredientJList.setFixedCellHeight(50);
        ingredientJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ingredientJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(ingredientJList);
        JLabel headerLabel = new JLabel("List of ingredients (Name):");

        this.add(headerLabel, BorderLayout.NORTH);
        this.add(pane, BorderLayout.CENTER);
    }

    private void prepareList() {
        ingredientListModel.removeAllElements();
        for (Ingredient ingredient : barDao.getIngredients()) {
            ingredientListModel.addElement(ingredient);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
