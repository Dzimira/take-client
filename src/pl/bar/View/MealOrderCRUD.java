package pl.bar.View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MealOrderCRUD extends JFrame {
    Dimension windowSize;
    GridBagConstraints gridBagConstraints;

    public MealOrderCRUD() {
        this.windowSize = new Dimension(400, 300);
        this.setTitle("Order CRUD");
        this.setSize(windowSize);
        this.gridBagConstraints = new GridBagConstraints();
        this.setLayout(new GridBagLayout());
        this.setVisible(true);
        this.centeringWindow();

        prepareGui();
    }

    private void prepareGui() {
        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(350, 200));

        JButton addBtn = new JButton("Add order");
        JButton getBtn = new JButton("Show order list");
        JButton findBtn = new JButton("Find order by id");
        JButton removeBtn = new JButton("Remove order");

        addBtn.setPreferredSize(new Dimension(300,30));
        getBtn.setPreferredSize(new Dimension(300,30));
        findBtn.setPreferredSize(new Dimension(300,30));
        removeBtn.setPreferredSize(new Dimension(300,30));

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CreateMealOrderFormView createMealOrderFormView = new CreateMealOrderFormView();
            }
        });

        getBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MealOrderListView mealOrderListView = new MealOrderListView();
            }
        });

        findBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                FindMealOrderByIdView findMealOrderByIdView = new FindMealOrderByIdView();
            }
        });

        removeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                RemoveMealOrderView removeMealOrderView = new RemoveMealOrderView();
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        buttonPanel.add(addBtn, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        buttonPanel.add(getBtn, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        buttonPanel.add(findBtn, gridBagConstraints);
        gridBagConstraints.gridy = 3;
        buttonPanel.add(removeBtn, gridBagConstraints);

        gridBagConstraints.gridy = 0;
        this.add(buttonPanel, gridBagConstraints);
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }

}
