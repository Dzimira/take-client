package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.ListCellRednderer;
import pl.bar.MealOrder;

import javax.swing.*;
import java.awt.*;

public class MealOrderListView extends JFrame{
    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<MealOrder> mealOrderJList;
    private DefaultListModel mealOrderListModel;
    private Bar barDao;
    JScrollPane pane;

    public MealOrderListView(){
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        prepareGui();
    }

    private void prepareGui() {
        this.mealOrderListModel = new DefaultListModel();
        prepareList();
        this.mealOrderJList = new JList<>(mealOrderListModel);
        this.mealOrderJList.setFixedCellHeight(50);
        mealOrderJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mealOrderJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(mealOrderJList);
        JLabel headerLabel = new JLabel("List of orders (Price):");

        this.add(headerLabel, BorderLayout.NORTH);
        this.add(pane, BorderLayout.CENTER);
    }

    private void prepareList() {
        mealOrderListModel.removeAllElements();
        for (MealOrder mealOrder : barDao.getMealOrders()) {
            mealOrderListModel.addElement(mealOrder);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }
}
