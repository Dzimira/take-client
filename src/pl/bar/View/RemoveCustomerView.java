package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.ListCellRednderer;
import pl.bar.Customer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveCustomerView extends JFrame{
    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<Customer> customerJList;
    private DefaultListModel customerListModel;
    private Bar barDao;
    JScrollPane pane;
    JButton removeCustomer;

    public RemoveCustomerView() {
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.removeCustomer = new JButton("Remove selected");
        this.setLayout(new GridBagLayout());
        prepareGui();
    }

    private void prepareGui() {

        this.customerListModel = new DefaultListModel();
        prepareList();
        this.customerJList = new JList<>(customerListModel);
        this.customerJList.setFixedCellHeight(50);
        customerJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        customerJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(customerJList);
        JLabel headerLabel = new JLabel("List of customers (Price | TableId):");

        removeCustomer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Customer customer = customerJList.getSelectedValue();

                barDao.deleteCustomer(customer.getId());
                customerListModel.removeElement(customer);
                refreshLists();

                JOptionPane.showMessageDialog(null, "Customer has been removed!");
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        this.add(headerLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(removeCustomer, gridBagConstraints);
    }

    private void prepareList() {
        customerListModel.removeAllElements();
        for (Customer customer : barDao.getCustomers()) {
            customerListModel.addElement(customer);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }

    private void refreshLists() {
        customerJList.revalidate();
        customerJList.repaint();
    }
}
