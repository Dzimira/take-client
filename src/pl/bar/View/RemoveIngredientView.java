package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.ListCellRednderer;
import pl.bar.Ingredient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveIngredientView extends JFrame{
    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<Ingredient> ingredientJList;
    private DefaultListModel ingredientListModel;
    private Bar barDao;
    JScrollPane pane;
    JButton removeIngredient;

    public RemoveIngredientView() {
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.removeIngredient = new JButton("Remove selected");
        this.setLayout(new GridBagLayout());
        prepareGui();
    }

    private void prepareGui() {

        this.ingredientListModel = new DefaultListModel();
        prepareList();
        this.ingredientJList = new JList<>(ingredientListModel);
        this.ingredientJList.setFixedCellHeight(50);
        ingredientJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        ingredientJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(ingredientJList);
        JLabel headerLabel = new JLabel("List of ingredients (Name):");

        removeIngredient.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Ingredient ingredient = ingredientJList.getSelectedValue();

                barDao.deleteIngredient(ingredient.getIdi());
                ingredientListModel.removeElement(ingredient);
                refreshLists();

                JOptionPane.showMessageDialog(null, "Ingredient has been removed!");
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        this.add(headerLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(removeIngredient, gridBagConstraints);
    }

    private void prepareList() {
        ingredientListModel.removeAllElements();
        for (Ingredient ingredient : barDao.getIngredients()) {
            ingredientListModel.addElement(ingredient);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }

    private void refreshLists() {
        ingredientJList.revalidate();
        ingredientJList.repaint();
    }
}
