package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.ListCellRednderer;
import pl.bar.MealOrder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveMealOrderView extends JFrame{
    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<MealOrder> mealOrderJList;
    private DefaultListModel mealOrderListModel;
    private Bar barDao;
    JScrollPane pane;
    JButton removeMealOrder;

    public RemoveMealOrderView() {
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.removeMealOrder = new JButton("Remove selected");
        this.setLayout(new GridBagLayout());
        prepareGui();
    }

    private void prepareGui() {

        this.mealOrderListModel = new DefaultListModel();
        prepareList();
        this.mealOrderJList = new JList<>(mealOrderListModel);
        this.mealOrderJList.setFixedCellHeight(50);
        mealOrderJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mealOrderJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(mealOrderJList);
        JLabel headerLabel = new JLabel("List of orders (Price):");

        removeMealOrder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MealOrder mealOrder = mealOrderJList.getSelectedValue();

                barDao.deleteMealOrder(mealOrder.getIdo());
                mealOrderListModel.removeElement(mealOrder);
                refreshLists();

                JOptionPane.showMessageDialog(null, "Order has been removed!");
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        this.add(headerLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(removeMealOrder, gridBagConstraints);
    }

    private void prepareList() {
        mealOrderListModel.removeAllElements();
        for (MealOrder mealOrder : barDao.getMealOrders()) {
            mealOrderListModel.addElement(mealOrder);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }

    private void refreshLists() {
        mealOrderJList.revalidate();
        mealOrderJList.repaint();
    }
}
