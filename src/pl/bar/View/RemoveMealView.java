package pl.bar.View;

import pl.bar.Bar;
import pl.bar.BarRemote;
import pl.bar.ListCellRednderer;
import pl.bar.Meal;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RemoveMealView extends JFrame {

    private GridBagConstraints gridBagConstraints;
    private Dimension windowSize;
    private JList<Meal> mealJList;
    private DefaultListModel mealListModel;
    private Bar barDao;
    JScrollPane pane;
    JButton removeMeal;

    public RemoveMealView() {
        this.barDao = new BarRemote();
        this.gridBagConstraints = new GridBagConstraints();
        this.setVisible(true);
        this.pane = new JScrollPane();
        this.windowSize = new Dimension(400, 500);
        this.setSize(this.windowSize);
        this.centeringWindow();
        this.removeMeal = new JButton("Remove selected");
        this.setLayout(new GridBagLayout());
        prepareGui();
    }

    private void prepareGui() {

        this.mealListModel = new DefaultListModel();
        prepareList();
        this.mealJList = new JList<>(mealListModel);
        this.mealJList.setFixedCellHeight(50);
        mealJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        mealJList.setCellRenderer(new ListCellRednderer());
        this.pane.setViewportView(mealJList);
        JLabel headerLabel = new JLabel("List of meals (Name | Description | Price | MealType):");

        removeMeal.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Meal meal = mealJList.getSelectedValue();

                barDao.deleteMeal(meal.getId());
                mealListModel.removeElement(meal);
                refreshLists();

                JOptionPane.showMessageDialog(null, "Meal has been removed!");
            }
        });

        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.gridy = 0;
        this.add(headerLabel, gridBagConstraints);
        gridBagConstraints.gridy = 1;
        this.add(pane, gridBagConstraints);
        gridBagConstraints.gridy = 2;
        this.add(removeMeal, gridBagConstraints);
    }

    private void prepareList() {
        mealListModel.removeAllElements();
        for (Meal meal : barDao.getMeals()) {
            mealListModel.addElement(meal);
        }
    }

    private void centeringWindow() {
        // place window in the center
        Dimension center = new Dimension(
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getWidth()) / 2,
                (int) (Toolkit.getDefaultToolkit().getScreenSize().getHeight()) / 2);
        this.setLocation(
                (int) (center.getWidth() - (this.windowSize.getWidth() / 2)),
                (int) (center.getHeight() - (this.windowSize.getHeight() / 2)));
    }

    private void refreshLists() {
        mealJList.revalidate();
        mealJList.repaint();
    }
}
